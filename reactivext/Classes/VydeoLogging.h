//
//  VydeoLogging.h
//  Pods
//
//  Created by Alexander Darovsky on 27.02.17.
//
//

#import <CocoaLumberjack/CocoaLumberjack.h>
#undef LOG_LEVEL_DEF
#define LOG_LEVEL_DEF vydeoLogLevel
extern const DDLogLevel vydeoLogLevel;
