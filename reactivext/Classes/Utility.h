//
//  Utility.h
//  Pods
//
//  Created by Alexander Darovsky on 03.03.17.
//
//
@import ReactiveObjC;

static inline BOOL isNull(id x)
{
    return x == nil || x == NSNull.null;
}

static inline BOOL isEmpty(NSString * s)
{
    return isNull(s) || s.length == 0;
}

static inline BOOL isBlank(NSString * s)
{
    return isEmpty(s) || [s stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0;
}

static inline NSString * nullToEmpty(id x) {
    return isNull(x) ? @"" : x;
}

static inline NSString * emptyToNil(NSString * x)
{
    return isEmpty(x) ? nil : x;
}

static inline id nullToNil(id x)
{
    return isNull(x) ? nil : x;
}

static inline id nilToNull(id x)
{
    return isNull(x) ? NSNull.null : x;
}

static inline BOOL isObjectDifferent(id a, id b)
{
    if (isNull(a) && isNull(b))
        return NO;
    if ((isNull(a) && !isNull(b)) ||
        (!isNull(a) && isNull(b)))
        return YES;
    return ![a isEqual:b];
}

static inline void runOnMainQueueWithoutDeadlocking(void (^block)(void))
{
    if ([NSThread isMainThread])
    {
        block();
    }
    else
    {
        dispatch_sync(dispatch_get_main_queue(), block);
    }
}

static inline id coalesceString(NSString * val, ...)
{
    if (!isEmpty(val))
        return val;

    va_list ap;
    va_start(ap, val);

    NSInteger argNo = 0;
    id obj = nil;
    while (isEmpty(obj)) {
        obj = va_arg(ap, id);
        if (++argNo > 10) {
            va_end(ap);
            @throw ([NSException exceptionWithName:@"Coalesce" reason:@"too deep coalesce stack, possibly wrong arguments" userInfo:nil]);
        }
    }
    va_end(ap);
    return obj;
}

CGFloat randomFloatBetween(CGFloat smallNumber, CGFloat bigNumber);
UIViewController * topViewController (UIViewController * viewController);
UITableViewCell * ownerCell(UIView * view);
UICollectionViewCell * ownerCollectionCell(UIView * view);
UITableView * ownerTable(UIView * view);


#define AUTO_PROPERTY(TYPE, MEMBER, INITIALIZER)                                \
- (TYPE*) MEMBER                                                                \
{                                                                               \
    if (_##MEMBER == nil)                                                       \
        self.MEMBER = INITIALIZER;                                              \
    return _##MEMBER;                                                           \
}

#define SESSION_PROP(prop)                                                      \
    [[[[[RACObserve(self, session)                                              \
     ignore:nil]                                                                \
     map:^id(VydeoSession * session) {                                          \
        @strongify(self);                                                       \
        return RACObserve(session, prop);                                       \
     }]                                                                         \
     switchToLatest]                                                            \
     distinctUntilChanged]                                                      \
     setNameWithFormat:@"SESSION:%s", #prop]

#define SESSION_STATES(...) \
    @[ metamacro_foreach(SESSION_STATE_value,, __VA_ARGS__) ]

#define SESSION_STATE_value(INDEX, ARG) \
    @(SessionState##ARG),

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface DateParser : NSObject

+ (NSDate *)getDate:(NSString *)dateString;
+ (NSString*)formatDate:(NSDate*)date;

/**
 * Parse duration in format of HH:mm:ss.sss
 * @return duration in seconds
 */
+ (NSTimeInterval) parseDuration:(NSString*)duration;
@end

@interface NSURL (TrailingSlash)

- (BOOL)hasTrailingSlash;
- (NSURL *)URLByAppendingTrailingSlash;

@end

@interface UIImage (Scaled)
- (UIImage *)imageResizedToSize:(CGSize)newSize;
@end

@interface NSString (Ranges)
@property (nonatomic, assign, readonly) NSRange wholeRange;
@end

@interface UIStoryboardSegue (CorrectedDestination)
- (__kindof UIViewController *) fixedDestinationViewController;
@end

@interface NSDictionary (NullReplacement)
- (NSDictionary *)dictionaryByReplacingNullsWithBlanks;
@end

@interface NSArray (NullReplacement)
- (NSArray *)arrayByReplacingNullsWithBlanks;
@end
