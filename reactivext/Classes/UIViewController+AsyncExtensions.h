//
//  UIViewController+AsyncExtensions.h
//  Pods
//
//  Created by Alexander Darovsky on 08.03.17.
//
//

@import UIKit;

@class RACSignal;
@interface UIViewController(AsyncExtensions)
- (RACSignal*) presentViewController:(UIViewController*)controller animated:(BOOL)animated;
- (RACSignal*) presentViewController:(UIViewController *)bottomViewController secondController:(UIViewController *)topViewController;
- (RACSignal*) dismissViewControllerAnimated:(BOOL)animated;
@end
