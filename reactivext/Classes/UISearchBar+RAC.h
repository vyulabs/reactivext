@import UIKit;
@class RACSignal;

@interface UISearchBar (RAC)
- (RACSignal *)rac_textSignal;
@end
