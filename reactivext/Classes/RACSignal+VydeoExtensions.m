//
//  RACSignal+VydeoExtensions.m
//  Pods
//
//  Created by Alexander Darovsky on 27.02.17.
//
//
#import <AFNetworking/AFNetworking.h>
#import <AFNetworking/UIKit+AFNetworking.h>
@import VydeoVisuals;
#import "VydeoLogging.h"
#import "Utility.h"
#import "RACSignal+VydeoExtensions.h"
#import "VVFancyAlertViewController+AsyncExtensions.h"

@implementation RACSignal(VydeoExtensions)
+ (RACSignal*)fetchImageFromURL:(NSURL*)url
{
    return [[[RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        RACScheduler * scheduler = [RACScheduler currentScheduler] ?: [RACScheduler mainThreadScheduler];
        RACCompoundDisposable *disposable = [RACCompoundDisposable compoundDisposable];

        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];

        AFImageDownloadReceipt * receipt =
        [[AFImageDownloader defaultInstance] downloadImageForURLRequest:request success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull responseObject) {
            [disposable addDisposable:[scheduler schedule:^{
                [subscriber sendNext:responseObject];
                [subscriber sendCompleted];
            }]];
        } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
            [disposable addDisposable:[scheduler schedule:^{
                [subscriber sendError:error];
            }]];
        }];


        [disposable addDisposable:[RACDisposable disposableWithBlock:^{
            if (receipt)
                [[AFImageDownloader defaultInstance] cancelTaskForImageDownloadReceipt:receipt];
        }]];

        return disposable;
    }]
    replayLazily]
    setNameWithFormat:@"fetchImageFromURL:(%@)", url];
}

- (RACSignal*) isIn:(id<NSFastEnumeration>)collection
{
    return [[self map:^id(id value) {
        for (id x in collection) {
            if ([value isEqual:x])
                return @YES;
        }

        return @NO;
    }] setNameWithFormat:@"[%@] -isIn:@[%@]", self.name, collection ];
}

- (RACSignal*) isNull
{
    return [[self map:^id(id value) {
        return @(isNull(value));
    }] setNameWithFormat:@"[%@] -isNull", self.name];
}

- (RACSignal*) isEmpty
{
    return [[self map:^id(id value) {
        return @(isNull(value) ||
                 ([value respondsToSelector:@selector(count)] && [value count] == 0) ||
                 ([value respondsToSelector:@selector(length)] && [value length] == 0));
    }] setNameWithFormat:@"[%@] -isEmpty", self.name];
}

- (RACSignal *)delayToDate:(NSDate*)date {
    return [[RACSignal createSignal:^(id<RACSubscriber> subscriber) {
        RACCompoundDisposable *disposable = [RACCompoundDisposable compoundDisposable];

        // We may never use this scheduler, but we need to set it up ahead of
        // time so that our scheduled blocks are run serially if we do.
        RACScheduler *scheduler = [RACScheduler scheduler];

        void (^schedule)(dispatch_block_t) = ^(dispatch_block_t block) {
            RACScheduler *delayScheduler = RACScheduler.currentScheduler ?: scheduler;
            RACDisposable *schedulerDisposable = [delayScheduler after:date schedule:block];
            [disposable addDisposable:schedulerDisposable];
        };

        RACDisposable *subscriptionDisposable = [self subscribeNext:^(id x) {
            schedule(^{
                [subscriber sendNext:x];
            });
        } error:^(NSError *error) {
            [subscriber sendError:error];
        } completed:^{
            schedule(^{
                [subscriber sendCompleted];
            });
        }];

        [disposable addDisposable:subscriptionDisposable];
        return disposable;
    }] setNameWithFormat:@"[%@] -delayToDate: %@", self.name, date];
}

- (RACSignal *)delayedRetry:(NSTimeInterval)interval {
    @weakify(self);
    return [[self
            catch:^(NSError *error) {
                if ([error.domain isEqualToString:@"ProtocolError"] && error.code == 2)
                    return [RACSignal error:error];

                @strongify(self);
                return [[[RACSignal
                        empty]
                        delay:interval]
                        concat:self ?: [RACSignal empty]];
            }]
            setNameWithFormat:@"[%@] -delayedRetry: %@", self.name, @(interval)];
}

- (RACSignal *)delayedRetry:(NSTimeInterval)interval limit:(NSInteger)limit {
    __block NSUInteger retryCount = 0;

    @weakify(self);
    return [self
            catch:^(NSError *error) {
                if (([error.domain isEqualToString:@"ProtocolError"] && error.code == 2) || ++retryCount > limit)
                    return [RACSignal error:error];

                @strongify(self);
                return [[[RACSignal
                        empty]
                        delay:interval]
                        concat:self ?: [RACSignal empty]];
            }];
}

- (RACSignal*) delayedPositive:(NSTimeInterval)interval
{
    return [[[[self
            distinctUntilChanged]
            map:^id(NSNumber * value) {
                if (value.boolValue) {
                    return [[[RACSignal empty]
                            delay:interval]
                            concat:[RACSignal return:value]];
                }
                else
                    return [RACSignal return:value];
            }]
            switchToLatest]
            setNameWithFormat:@"[%@] delayedPositive:(%.2f)", self.name, interval];
}

- (RACSignal*) delayedNegative:(NSTimeInterval)interval
{
    return [[[[self
            distinctUntilChanged]
            map:^id(NSNumber * value) {
                if (!value.boolValue) {
                    return [[[RACSignal empty]
                             delay:interval]
                            concat:[RACSignal return:value]];
                }
                else
                    return [RACSignal return:value];
            }]
            switchToLatest]
            setNameWithFormat:@"[%@] delayedPositive:(%.2f)", self.name, interval];
}

+ (RACSignal*) countdownToDate:(NSDate*)timeout
{
    NSDate * now = [NSDate date];
    NSTimeInterval length = [timeout timeIntervalSinceDate:now];
    NSTimeInterval rounded = length - floor(length); // franctional part of second to start with
    NSDate * startDate = [NSDate dateWithTimeIntervalSinceNow:rounded];

    RACScheduler * scheduler = RACScheduler.mainThreadScheduler;
    RACSignal * interval = [[RACSignal createSignal:^(id<RACSubscriber> subscriber) {
        return [scheduler after:startDate repeatingEvery:1 withLeeway:0 schedule:^{
            [subscriber sendNext:[NSDate date]];
        }];
    }] setNameWithFormat:@"+intervalAligned"];

    return [[[interval
            startWith:[NSDate date]]
            takeWhileBlock:^BOOL(NSDate * cur) {
                return [cur compare:timeout] == NSOrderedAscending;
            }]
            map:^id(NSDate * cur) {
                NSTimeInterval t = -[cur timeIntervalSinceDate:timeout];
                return @(t);
            }];
}

- (RACSignal *)failOnEmpty
{
    return [self flattenMap:^RACSignal *(NSArray * arr) {
        if (arr.count == 0)
            return [RACSignal error:[NSError errorWithDomain:@"APP" code:502 userInfo:nil]];
        else
            return [RACSignal return:arr];
    }];
}

- (RACSignal *)failOnFalse
{
    return [self flattenMap:^RACSignal *(NSNumber * value) {
        if (!value.boolValue)
            return [RACSignal error:[NSError errorWithDomain:@"APP" code:502 userInfo:nil]];
        else
            return [RACSignal return:value];
    }];
}


- (RACSignal *)animateWithDuration:(NSTimeInterval)duration options:(UIViewAnimationOptions)options {
    return [[self
             map:^(id value) {
                 return [RACSignal createSignal:^ id (id<RACSubscriber> subscriber) {
                     [UIView animateWithDuration:duration delay:0 options:options animations:^{
                         [subscriber sendNext:value];
                     } completion:^(BOOL finished) {
                         [subscriber sendCompleted];
                     }];

                     return nil;
                 }];
             }]
            concat];
}

- (RACSignal *)animateWithDuration:(NSTimeInterval)duration {
    return [self animateWithDuration:duration options:0];
}

- (RACSignal *)transitionAnimationInView:(UIView*)view duration:(NSTimeInterval)duration options:(UIViewAnimationOptions)options {
    @weakify(view);
    return [[self
             map:^(id value) {
                 return [RACSignal createSignal:^ id (id<RACSubscriber> subscriber) {
                     @strongify(view);
                     if (view) {
                         [UIView transitionWithView:view duration:duration options:options animations:^{
                             [subscriber sendNext:value];
                         } completion:^(BOOL finished) {
                             [subscriber sendCompleted];
                         }];
                     }
                     else {
                         [subscriber sendCompleted];
                     }
                     return nil;
                 }];
             }]
            concat];
}

- (RACSignal*) mapThen:(id)value
{
    return [self then:^RACSignal *{
        return [RACSignal return:value];
    }];
}

- (RACSignal *)doAfterNext:(void (^)(id x))block {
    NSCParameterAssert(block != NULL);

    return [[RACSignal createSignal:^(id<RACSubscriber> subscriber) {
        return [self subscribeNext:^(id x) {
            [subscriber sendNext:x];
            block(x);
        } error:^(NSError *error) {
            [subscriber sendError:error];
        } completed:^{
            [subscriber sendCompleted];
        }];
    }] setNameWithFormat:@"[%@] -doNext:", self.name];
}

- (RACSignal*) displayErrorInController:(UIViewController*)controller
                         defaultMessage:(NSString*)message
{
    @weakify(controller);
    return [self catch:^RACSignal *(NSError *error) {
        @strongify(controller);
        if (controller) {
            NSString * text = [error.domain isEqualToString:@"Logic"] ||
                              [error.domain hasPrefix:@"Vydeo"] ||
                              [error.domain isEqualToString:@"AuthError"] ? error.localizedDescription : message;


            return [[VVFancyAlertViewController
                    fancyAlertInViewController:controller
                    title:nil
                    message:text
                    okLabel:NSLocalizedString(@"OK", @"displayError: button title")]
                    flattenMap:^RACSignal *(id value) {
                        return [RACSignal error:error];
                    }];
        }
        else {
            return [RACSignal error:error];
        }
    }];
}

- (RACSignal *)boolWithTrue:(id)trueValue andFalse:(id)falseValue
{
    return [[self map:^id(NSNumber * boolVal) {
        return boolVal.boolValue ? trueValue : falseValue;
    }] setNameWithFormat:@"[%@] boolWithTrue:(%@) andFalse:(%@)", self.name, trueValue, falseValue];
}

- (instancetype)distinctUntilChangedWithComparator:(BOOL (^)(id, id))comparator {
    Class class = self.class;

    return [[self bind:^{
        __block id lastValue = nil;
        __block BOOL initial = YES;

        return ^(id x, BOOL *stop) {
            if (!initial && comparator(lastValue, x)) return [class empty];

            initial = NO;
            lastValue = x;
            return [class return:x];
        };
    }] setNameWithFormat:@"[%@] -distinctUntilChangedWithComparator", self.name];
}

- (RACDisposable*) subscribeCallback:(void(^)(NSError * error, id result))callback
{
    BOOL __block called = NO;
    return [[self take:1] subscribeNext:^(id x) {
        called = YES;
        if (callback)
            callback(nil, x);
    } error:^(NSError *error) {
        if (callback)
            callback(error, nil);
    }
    completed:^{
        if (!called && callback)
            callback(nil, nil);
    }];
}

- (RACDisposable*) subscribeCallbackNoResult:(void(^)(NSError * error))callback
{
    return [[self take:1]
            subscribeError:^(NSError *error) {
                callback(error);
            }
            completed:^{
                callback(nil);
            }];
}

- (RACDisposable*) subscribeFinally:(void (^)())callback
{
    return [[self take:1]
            subscribeError:^(NSError *error) {
                callback();
            }
            completed:^{
                callback();
            }];
}

- (RACSignal *)ddLogAll {
    return [[[self ddLogNext] ddLogError] ddLogCompleted];
}

- (RACSignal *)ddLogNext {
    return [[self doNext:^(id x) {
        DDLogDebug(@"%@ next: %@", self, x);
    }] setNameWithFormat:@"%@", self.name];
}

- (RACSignal *)ddLogError {
    return [[self doError:^(NSError *error) {
        DDLogError(@"%@ error: %@", self, error);
    }] setNameWithFormat:@"%@", self.name];
}

- (RACSignal *)ddLogCompleted {
    return [[self doCompleted:^{
        DDLogDebug(@"%@ completed", self);
    }] setNameWithFormat:@"%@", self.name];
}
@end
