//
//  RACSignal+VydeoExtensions.h
//  Pods
//
//  Created by Alexander Darovsky on 27.02.17.
//
//

#import <Foundation/Foundation.h>
#import <ReactiveObjC/ReactiveObjC.h>

@interface RACSignal<__covariant ValueType>(VydeoExtensions)
/**
 fetches an image from the given URL. If error occurs, reports an error using sendError
 @return an UIImage fetched from the given URL
 */
+ (RACSignal<UIImage*>*) fetchImageFromURL:(NSURL*)url;

/**
 converts signal's NSNumber value to boolean

 @return true if signal's value is in provided collection, false otherwise
 */
- (RACSignal<NSNumber*> *) isIn:(id<NSFastEnumeration>)collection;

/** Converts signal to boolean
 @return true, if next value is null, false otherwise
 */
- (RACSignal<NSNumber*> *) isNull;

/** Converts signal to boolean
 @return true, if next value is empty, false otherwise
 */
- (RACSignal<NSNumber*> *) isEmpty;

- (RACSignal<ValueType> *)delayToDate:(NSDate*)date;

- (RACSignal<ValueType> *)delayedRetry:(NSTimeInterval)interval;

- (RACSignal<ValueType> *)delayedRetry:(NSTimeInterval)interval limit:(NSInteger)limit;

- (RACSignal<NSNumber*> *) delayedPositive:(NSTimeInterval)interval;
- (RACSignal<NSNumber*> *) delayedNegative:(NSTimeInterval)interval;

+ (RACSignal<NSNumber*> *) countdownToDate:(NSDate*)timeout;

- (RACSignal<ValueType> *)animateWithDuration:(NSTimeInterval)duration options:(UIViewAnimationOptions)options;
- (RACSignal<ValueType> *)animateWithDuration:(NSTimeInterval)duration;
- (RACSignal<ValueType> *)transitionAnimationInView:(UIView*)view duration:(NSTimeInterval)duration options:(UIViewAnimationOptions)options;

- (RACSignal *)mapThen:(id)value;

- (RACSignal<ValueType> *)doAfterNext:(void (^)(id x))block;

- (RACSignal *)boolWithTrue:(id)trueValue andFalse:(id)falseValue;

- (RACSignal *)failOnEmpty;
- (RACSignal *)failOnFalse;
- (RACSignal*) displayErrorInController:(UIViewController*)controller
                         defaultMessage:(NSString*)message;

- (instancetype)distinctUntilChangedWithComparator:(BOOL (^)(id, id))comparator;

/** Convert Reactive version to callback one
 */
- (RACDisposable*) subscribeCallback:(void(^)(NSError * error, id result))callback;
- (RACDisposable*) subscribeCallbackNoResult:(void(^)(NSError * error))callback;
- (RACDisposable*) subscribeFinally:(void (^)())callback;

- (RACSignal *)ddLogAll;
- (RACSignal *)ddLogNext;
- (RACSignal *)ddLogError;
- (RACSignal *)ddLogCompleted;
@end
