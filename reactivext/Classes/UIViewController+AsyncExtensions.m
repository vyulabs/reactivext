//
//  UIViewController+AsyncExtensions.m
//  Pods
//
//  Created by Alexander Darovsky on 08.03.17.
//
//
@import ReactiveObjC;

#import "UIViewController+AsyncExtensions.h"

@implementation UIViewController(AsyncExtensions)
- (void) presentViewController:(UIViewController *)bottomViewController
              secondController:(UIViewController *)topViewController
                    completion:(void (^)(void))completion
{
    UIGraphicsBeginImageContextWithOptions(self.view.window.bounds.size, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [self.view.window.layer renderInContext:context];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.view.window.frame];
    imageView.image = image;
    [self.view.window insertSubview:imageView atIndex:0];

    // Present the VCs
//    bottomViewController.modalPresentationStyle = UIModalPresentationOverFullScreen;
    bottomViewController.view.alpha = 0; // Set alpha to zero so that we can see the imageView
    [self presentViewController:bottomViewController animated:NO completion:^{
        [bottomViewController presentViewController:topViewController animated:YES completion:^{
            bottomViewController.view.alpha = 1;
            [imageView removeFromSuperview];

            if (completion)
                completion();
        }];
    }];
}

- (RACSignal*) presentViewController:(UIViewController*)controller animated:(BOOL)animated
{
    RACReplaySubject * result = [RACReplaySubject replaySubjectWithCapacity:1];
    [self presentViewController:controller animated:animated completion:^{
        [result sendCompleted];
    }];

    return result;
}

- (RACSignal*) presentViewController:(UIViewController *)bottomViewController secondController:(UIViewController *)topViewController
{
    RACReplaySubject * result = [RACReplaySubject replaySubjectWithCapacity:1];

    [self presentViewController:bottomViewController secondController:topViewController completion:^{
        [result sendCompleted];
    }];

    return result;
}

- (RACSignal*) dismissViewControllerAnimated:(BOOL)animated
{
    RACReplaySubject * result = [RACReplaySubject replaySubjectWithCapacity:1];
    [self dismissViewControllerAnimated:animated completion:^{
        [result sendCompleted];
    }];

    return result;
}

@end
