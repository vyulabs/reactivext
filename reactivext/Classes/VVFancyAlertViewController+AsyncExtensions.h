//
//  FancyAlertViewController+AsyncExtensions.h
//  Pods
//
//  Created by Alexander Darovsky on 03.05.17.
//
//

#import <Foundation/Foundation.h>
#import <ReactiveObjC/ReactiveObjC.h>
#import <VydeoVisuals/VVFancyAlertViewController.h>

@interface VVFancyAlertViewController(AsyncExtensions)
+ (RACSignal*) fancyAlertInViewController:(UIViewController *)viewController
                                    title:(NSString *)title
                                  message:(NSString *)message
                        buttonOrientation:(UILayoutConstraintAxis)orientation
                                  yesIcon:(UIImage *)yesIcon
                                 yesLabel:(NSString *)yesLabel
                                   noIcon:(UIImage *)noIcon
                                  noLabel:(NSString *)noLabel;
+ (RACSignal*) fancyAlertInViewController:(UIViewController *)viewController
                                    title:(NSString *)title
                                  message:(NSString *)message
                                  okLabel:(NSString *)yesLabel;
+ (RACSignal *)fancyAlertInViewController:(UIViewController*)viewController
                               titleImage:(UIImage *)titleImage
                                  message:(NSString*)message
                        buttonOrientation:(UILayoutConstraintAxis)orientation
                             buttonTitles:(NSArray<NSString*>*)labels;
@end
