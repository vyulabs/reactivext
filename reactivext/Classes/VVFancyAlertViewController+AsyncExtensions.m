//
//  FancyAlertViewController+AsyncExtensions.m
//  Pods
//
//  Created by Alexander Darovsky on 03.05.17.
//
//
@import VydeoVisuals;

#import "VVFancyAlertViewController+AsyncExtensions.h"

@implementation VVFancyAlertViewController(AsyncExtensions)
+ (RACSignal*) fancyAlertInViewController:(UIViewController *)viewController
                                    title:(NSString *)title message:(NSString *)message
                        buttonOrientation:(UILayoutConstraintAxis)orientation
                                  yesIcon:(UIImage *)yesIcon yesLabel:(NSString *)yesLabel
                                   noIcon:(UIImage *)noIcon noLabel:(NSString *)noLabel
{
    return [[RACSignal startEagerlyWithScheduler:[RACScheduler mainThreadScheduler] block:^(id<RACSubscriber> subscriber) {
        VVFancyAlertViewController * alert = [[VVFancyAlertViewController alloc]
                                            initWithTitle:title
                                            message:message
                                            buttonOrientation:orientation];

        [viewController presentViewController:alert animated:YES completion:nil];

        UIButton * b;
        b = [alert addButtonWithImage:nil title:noLabel ?: NSLocalizedString(@"No", @"General: yes/no alert: no title") block:^{
            [subscriber sendNext:@NO];
            [subscriber sendCompleted];
        }];
        b.tintColor = [UIColor whiteColor];
        [b setTitleColor:viewController.view.tintColor forState:UIControlStateNormal];

        b = [alert addButtonWithImage:nil title:yesLabel ?: NSLocalizedString(@"Yes", @"General: yes/no alert: yes title") block:^{
            [subscriber sendNext:@YES];
            [subscriber sendCompleted];
        }];
    }]
    setNameWithFormat:@"fancyAlertViewController:(%@) title: (%@) message:(%@) okLabel:(%@)",
    viewController, title, message, yesLabel];
}

+ (RACSignal *)alertInViewController:(UIViewController*)viewController
                               style:(UIAlertControllerStyle)style
                               title:(NSString*)title
                             message:(NSString*)message
                         cancelTitle:(NSString*)cancelTitle // defaults to Cancel
                        buttonTitles:(NSArray<NSString*>*)labels // defaults to No if nil
{
    return [[RACSignal startEagerlyWithScheduler:[RACScheduler mainThreadScheduler] block:^(id<RACSubscriber> subscriber) {
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:title
                                                                        message:message
                                                                 preferredStyle:style];
        [alert addAction:[UIAlertAction actionWithTitle:cancelTitle ?: NSLocalizedString(@"Cancel", @"General: alert: cancel title")
                                                  style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                                                      [subscriber sendNext:@0];
                                                      [subscriber sendCompleted];
                                                  }]];
        [labels enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [alert addAction:[UIAlertAction actionWithTitle:obj
                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                                                          [subscriber sendNext:@(idx+1)];
                                                          [subscriber sendCompleted];
                                                      }]];
        }];
        [viewController presentViewController:alert animated:YES completion:nil];
    }]
    setNameWithFormat:@"fancyAlertViewController:(%@) title: (%@) message:(%@) cancelTitle:(%@) buttonTitles:(%@)",
    viewController, title, message, cancelTitle, labels];
}

+ (RACSignal *)fancyAlertInViewController:(UIViewController*)viewController
                               titleImage:(UIImage *)titleImage
                                  message:(NSString*)message
                        buttonOrientation:(UILayoutConstraintAxis)orientation
                             buttonTitles:(NSArray<NSString*>*)labels
{
    return [[RACSignal startEagerlyWithScheduler:[RACScheduler mainThreadScheduler] block:^(id<RACSubscriber> subscriber) {
        VVFancyAlertViewController * alert = [[VVFancyAlertViewController alloc]
                                            initWithImage:titleImage
                                            title:nil
                                            message:message
                                            buttonOrientation:orientation];

        [labels enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [alert addButtonWithImage:nil empty:(idx > 0) title:obj block:^{
                [subscriber sendNext:@(idx)];
                [subscriber sendCompleted];
            }];
        }];

        [viewController presentViewController:alert animated:YES completion:nil];
    }]
    setNameWithFormat:@"fancyAlertViewController:(%@) message:(%@) buttonTitles:(%@)",
    viewController, message, labels];
}

+ (RACSignal*) fancyAlertInViewController:(UIViewController *)viewController
                                    title:(NSString *)title
                                  message:(NSString *)message
                                  okLabel:(NSString *)yesLabel
{
    return [[RACSignal startEagerlyWithScheduler:[RACScheduler mainThreadScheduler] block:^(id<RACSubscriber> subscriber) {
        VVFancyAlertViewController * alert = [[VVFancyAlertViewController alloc]
                                            initWithTitle:title
                                            message:message
                                            buttonOrientation:UILayoutConstraintAxisHorizontal];

        [viewController presentViewController:alert animated:YES completion:nil];

        [alert addButtonWithImage:nil title:yesLabel ?: NSLocalizedString(@"Dismiss", @"General: yes/no alert: no title") block:^{
            [subscriber sendNext:@YES];
            [subscriber sendCompleted];
        }];
    }] setNameWithFormat:@"fancyAlertViewController:(%@) title: (%@) message:(%@) okLabel:(%@)",
            viewController, title, message, yesLabel];
}

@end
