//
//  Utility.m
//  Pods
//
//  Created by Alexander Darovsky on 03.03.17.
//
//

#import "Utility.h"

static NSString *zuluTimeFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
static NSDateFormatter *zuluFormatter;

CGFloat randomFloatBetween(CGFloat smallNumber, CGFloat bigNumber)
{
    CGFloat diff = bigNumber - smallNumber;
    return (((CGFloat) (arc4random() % ((unsigned)RAND_MAX + 1)) / RAND_MAX) * diff) + smallNumber;
}

UIViewController * topViewController (UIViewController * viewController)
{
    if (viewController.presentedViewController)
        return topViewController(viewController.presentedViewController);
    if ([viewController isKindOfClass:[UINavigationController class]])
        return topViewController([(UINavigationController*)viewController visibleViewController]);
    if (viewController.navigationController && [viewController.navigationController visibleViewController] != viewController)
        return topViewController([viewController.navigationController visibleViewController]);
    return viewController;
}

UITableViewCell * ownerCell(UIView * view)
{
    UIView * parent = view;
    while (parent != nil) {
        if ([parent isKindOfClass:[UITableViewCell class]])
            return (UITableViewCell*)parent;
        parent = parent.superview;
    }
    return nil;
}

UICollectionViewCell * ownerCollectionCell(UIView * view)
{
    UIView * parent = view;
    while (parent != nil) {
        if ([parent isKindOfClass:[UICollectionViewCell class]])
            return (UICollectionViewCell*)parent;
        parent = parent.superview;
    }
    return nil;
}

UITableView * ownerTable(UIView * view)
{
    UIView * parent = view;
    while (parent != nil) {
        if ([parent isKindOfClass:[UITableView class]])
            return (UITableView*)parent;
        parent = parent.superview;
    }
    return nil;
}

@implementation DateParser

+ (void)initialize {
    NSLocale *properLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    zuluFormatter = [[NSDateFormatter alloc] init];
    [zuluFormatter setLocale:properLocale];
    [zuluFormatter setDateFormat:zuluTimeFormat];
    [zuluFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
}

+ (NSDate *)getDate:(NSString *)dateString {
    if ([dateString hasSuffix:@"Z"]) {
        NSDate *result = [zuluFormatter dateFromString:dateString];
        return result;
    } else { // TODO: this should be parsing the time with offset
        return nil;
    }
}

+ (NSString*)formatDate:(NSDate *)date
{
    return [zuluFormatter stringFromDate:date];
}

+ (NSTimeInterval) parseDuration:(NSString*)duration
{
    NSTimeInterval result = 0;
    NSInteger x;
    NSScanner * scanner = [[NSScanner alloc] initWithString:duration];

    // hours
    if (![scanner scanInteger:&x])
        return -1;
    result += x * 3600;

    // delimiter
    if (![scanner scanString:@":" intoString:nil])
        return -1;

    // minutes
    if (![scanner scanInteger:&x])
        return -1;
    result += x * 60;

    // delimiter
    if (![scanner scanString:@":" intoString:nil])
        return -1;

    // seconds
    if (![scanner scanInteger:&x])
        return -1;
    result += x;

    // milliseconds
    if ([scanner scanString:@"." intoString:nil]) {
        if (![scanner scanInteger:&x])
            return -1;
        result += (x / 1000.0);
    }

    return result;
}

@end

@implementation NSURL (TrailingSlash)

- (BOOL)hasTrailingSlash {
    return [self.absoluteString hasSuffix:@"/"];
}

- (NSURL *)URLByAppendingTrailingSlash {
    if (!self.hasTrailingSlash)
        return [self URLByAppendingPathComponent:@""];
    else
        return self;
}

@end

@implementation UIImage (Scaled)
- (UIImage *)imageResizedToSize:(CGSize)newSize {
    CGFloat scaleWidth = 1.0f;
    CGFloat scaleHeight = 1.0f;

    if (CGSizeEqualToSize(self.size, newSize) == NO) {

        //calculate "the longer side"
        if(self.size.width > self.size.height) {
            scaleWidth = self.size.width / self.size.height;
        } else {
            scaleHeight = self.size.height / self.size.width;
        }
    }

    //prepare source and target image
    UIImage *sourceImage = self;
    UIImage *newImage = nil;

    //now we create a context in newSize and draw the image out of the bounds of the context to get
    //an proportionally scaled image by cutting of the image overlay
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0f);

    //Center image point so that on each egde is a little cutoff
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.size.width  = newSize.width * scaleWidth;
    thumbnailRect.size.height = newSize.height * scaleHeight;
    thumbnailRect.origin.x = (int) (newSize.width - thumbnailRect.size.width) * 0.5;
    thumbnailRect.origin.y = (int) (newSize.height - thumbnailRect.size.height) * 0.5;

    [sourceImage drawInRect:thumbnailRect];

    newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return newImage;
}
@end

@implementation NSString (Ranges)

- (NSRange) wholeRange
{
    return NSMakeRange(0, self.length);
}

@end

@implementation UIStoryboardSegue (CorrectedDestination)
- (__kindof UIViewController *) fixedDestinationViewController
{
    if ([self.destinationViewController isKindOfClass:[UINavigationController class]]) {
        return [(UINavigationController*)self.destinationViewController topViewController];
    }
    else
        return self.destinationViewController;
}
@end

@implementation NSDictionary (NullReplacement)

- (NSDictionary *)dictionaryByReplacingNullsWithBlanks {
    const NSMutableDictionary *replaced = [self mutableCopy];
    const id nul = [NSNull null];
    for (NSString *key in self) {
        id object = [self objectForKey:key];
        if (object == nul) [replaced setObject:nullToEmpty(object) forKey:key];
        else if ([object isKindOfClass:[NSDictionary class]]) [replaced setObject:[object dictionaryByReplacingNullsWithBlanks] forKey:key];
        else if ([object isKindOfClass:[NSArray class]]) [replaced setObject:[object arrayByReplacingNullsWithBlanks] forKey:key];
    }
    return [NSDictionary dictionaryWithDictionary:[replaced copy]];
}

@end

@implementation NSArray (NullReplacement)

- (NSArray *)arrayByReplacingNullsWithBlanks  {
    NSMutableArray *replaced = [self mutableCopy];
    const id nul = [NSNull null];
    for (int idx = 0; idx < [replaced count]; idx++) {
        id object = [replaced objectAtIndex:idx];
        if (object == nul) [replaced replaceObjectAtIndex:idx withObject:nullToEmpty(object)];
        else if ([object isKindOfClass:[NSDictionary class]]) [replaced replaceObjectAtIndex:idx withObject:[object dictionaryByReplacingNullsWithBlanks]];
        else if ([object isKindOfClass:[NSArray class]]) [replaced replaceObjectAtIndex:idx withObject:[object arrayByReplacingNullsWithBlanks]];
    }
    return [replaced copy];
}

@end