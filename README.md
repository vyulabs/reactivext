# reactivext

[![CI Status](http://img.shields.io/travis/adarovsky/reactivext.svg?style=flat)](https://travis-ci.org/adarovsky/reactivext)
[![Version](https://img.shields.io/cocoapods/v/reactivext.svg?style=flat)](http://cocoapods.org/pods/reactivext)
[![License](https://img.shields.io/cocoapods/l/reactivext.svg?style=flat)](http://cocoapods.org/pods/reactivext)
[![Platform](https://img.shields.io/cocoapods/p/reactivext.svg?style=flat)](http://cocoapods.org/pods/reactivext)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

reactivext is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "reactivext"
```

## Author

adarovsky, adarovsky@vyulabs.com

## License

reactivext is available under the MIT license. See the LICENSE file for more info.
