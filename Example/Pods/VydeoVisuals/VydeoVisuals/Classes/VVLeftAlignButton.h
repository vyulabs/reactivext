//
//  LeftAlignButton.h
//  LeftAlignButton
//
//  Created by Alexander Darovsky on 09.02.16.
//  Copyright © 2016 Vyulabs. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface VVLeftAlignButton : UIButton

@end
