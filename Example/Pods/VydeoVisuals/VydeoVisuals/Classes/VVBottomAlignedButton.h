//
//  BottomAlignedButton.h
//  FanDate
//
//  Created by Alexander Darovsky on 07.03.16.
//  Copyright © 2016 Vyu Labs, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface VVBottomAlignedButton : UIButton
@property (nonatomic, assign) IBInspectable CGFloat topPadding;

@end
