//
//  LeftAlignButton.m
//  LeftAlignButton
//
//  Created by Alexander Darovsky on 09.02.16.
//  Copyright © 2016 Vyulabs. All rights reserved.
//

#import "VVLeftAlignButton.h"

@implementation VVLeftAlignButton

- (CGRect) imageRectForContentRect:(CGRect)contentRect
{
//    DDLogDebug(@"content rect: %@", NSStringFromCGRect(contentRect));
    CGRect imageRect = [super imageRectForContentRect:contentRect];
//    DDLogDebug(@"image rect: %@", NSStringFromCGRect(imageRect));
    imageRect.origin.x = contentRect.origin.x + self.imageEdgeInsets.left;
//    DDLogDebug(@"new image rect: %@", NSStringFromCGRect(imageRect));
    return imageRect;
}
@end
