//
//  CameraPermissionViewController.m
//  FanDate
//
//  Created by Alexander Darovsky on 05.02.16.
//  Copyright © 2016 Vyu Labs, Inc. All rights reserved.
//
#import <VydeoVisuals/VVLeftAlignButton.h>
@import AVFoundation;

#import "VVFancyAlertViewController.h"
#import "UIImage+RoundRect.h"

@interface VVFancyAlertViewController ()
@property (weak, nonatomic) IBOutlet UIView *titleImageContainer;
@property (weak, nonatomic) IBOutlet UIImageView *titleImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *titleSeparator;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomSpace;

@property (nonatomic, assign) UILayoutConstraintAxis orientation;
@property (weak, nonatomic) IBOutlet UIStackView *stackView;
@property (nonatomic, strong) UIImage * internalImage;
@property (nonatomic, strong) NSString * internalTitle;
@property (nonatomic, strong) NSString * internalMessage;
@property (nonatomic, strong) NSMutableArray<NSDictionary*> * buttons;
@end

static inline BOOL isNull(id x)
{
    return x == nil || x == NSNull.null;
}

static inline BOOL isEmpty(NSString * s)
{
    return isNull(s) || s.length == 0;
}

static inline BOOL isBlank(NSString * s)
{
    return isEmpty(s) || [s stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0;
}


@implementation VVFancyAlertViewController

- (instancetype) initWithImage:(UIImage*)image
                         title:(NSString*)title
                       message:(NSString*)message
             buttonOrientation:(UILayoutConstraintAxis)orientation
{
    NSBundle * podBundle = [NSBundle bundleForClass:[self class]];
    NSURL * u = [podBundle URLForResource:@"VydeoVisuals" withExtension:@"bundle"];
    NSBundle * resourceBundle = [NSBundle bundleWithURL:u];

    if (self = [super initWithNibName:@"FancyAlertViewController" bundle:resourceBundle]) {
        _internalImage = image;
        _internalMessage = message;
        _internalTitle = title;
        _orientation = orientation;
        _buttons = [NSMutableArray arrayWithCapacity:3];
        self.modalPresentationStyle = UIModalPresentationOverFullScreen;
        self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    }

    return self;
}

- (instancetype) initWithTitle:(NSString*)title
                       message:(NSString*)message
             buttonOrientation:(UILayoutConstraintAxis)orientation
{
    NSBundle * podBundle = [NSBundle bundleForClass:[self class]];
    NSURL * u = [podBundle URLForResource:@"VydeoVisuals" withExtension:@"bundle"];
    NSBundle * resourceBundle = [NSBundle bundleWithURL:u];

    if (self = [super initWithNibName:@"FancyAlertViewController" bundle:resourceBundle]) {
        _internalImage = nil;
        _internalMessage = message;
        _internalTitle = title;
        _orientation = orientation;
        _buttons = [NSMutableArray arrayWithCapacity:3];
        self.modalPresentationStyle = UIModalPresentationOverFullScreen;
        self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    }

    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.titleImageContainer.hidden = self.internalImage == nil;
    self.titleImage.image = self.internalImage;

    self.stackView.axis = self.orientation;
    self.stackView.distribution = UIStackViewDistributionFillEqually;

    self.titleLabel.text = self.internalTitle;
    self.messageLabel.text = self.internalMessage;

    if (self.internalTitle == nil || self.internalTitle.length == 0) {
        self.titleLabel.hidden = YES;
        self.titleSeparator.hidden = YES;
    }

    for (NSDictionary * t in self.buttons)
        [self _addButtonWithImage:t[@"image"]
                            empty:[t[@"empty"] boolValue]
                            title:t[@"title"]
                            block:t[@"block"]];
}

- (UIButton*) _addButtonWithImage:(UIImage *)image
                            empty:(BOOL)empty
                            title:(NSString *)title
                            block:(void (^)())block
{
    UIButton * button = [VVLeftAlignButton buttonWithType:UIButtonTypeCustom];

    if (empty) {
        [button setTitleColor:button.tintColor
                     forState:UIControlStateNormal];
        [button setBackgroundImage:[[UIImage roundRectWithCorner:4
                                                 filledWithColor:[UIColor clearColor]
                                                strokedWithColor:[UIColor blackColor]
                                                       lineWidth:2]
                                    imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]
                          forState:UIControlStateNormal];
    }
    else {
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [button setBackgroundImage:[[UIImage roundRectWithCorner:4 tintedWithColor:[UIColor blackColor]]
                                    imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]
                          forState:UIControlStateNormal];
    }
    button.contentEdgeInsets = UIEdgeInsetsMake(10, 15, 10, 15);
    button.titleLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    if (!isBlank(title))
        [button setTitle:title forState:UIControlStateNormal];

    if (!isNull(image))
        [button setImage:image forState:UIControlStateNormal];

    button.tag = 100 + self.stackView.arrangedSubviews.count;
    [self.stackView addArrangedSubview:button];
    [button addTarget:self action:@selector(_didClickButton:) forControlEvents:UIControlEventTouchUpInside];

    self.bottomSpace.constant = empty ? 10 : 20;

    return button;
}

- (UIButton*) addButtonWithImage:(UIImage *)image title:(NSString *)title block:(void(^)())block
{
    return [self addButtonWithImage:image empty:NO title:title block:block];
}

- (UIButton*) addButtonWithImage:(UIImage *)image empty:(BOOL)empty title:(NSString *)title block:(void (^)())block
{
    id b = [block ?: ^{} copy];
    [self.buttons addObject:@{@"image": image ?: NSNull.null,
                              @"title": title ?: NSNull.null,
                              @"block": b,
                              @"empty": @(empty)}];
    if ([self isViewLoaded]) {
        return [self _addButtonWithImage:image empty:empty title:title block:block];
    }
    return nil;
}

- (void) _didClickButton:(UIButton*)button
{
    NSInteger pos = button.tag - 100;

    void (^block)() = self.buttons[pos][@"block"];
    NSParameterAssert(block);

    [self.presentingViewController dismissViewControllerAnimated:YES completion:^{
        block();
    }];
}
@end

