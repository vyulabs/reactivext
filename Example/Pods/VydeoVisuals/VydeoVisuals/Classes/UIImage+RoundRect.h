//
//  UIImage+RoundRect.h
//  Pods
//
//  Created by Alexander Darovsky on 03.05.17.
//
//

#import <UIKit/UIKit.h>

@interface UIImage(VydeoUtility)
- (UIImage*) roundImage;
- (UIImage*) roundImageWithBorder:(CGFloat)width ofColor:(UIColor*)color;
+ (UIImage*) circleWithDiameter:(CGFloat)diameter tintedWithColor:(UIColor*)color;
+ (UIImage*) roundRectWithCorner:(CGFloat)radius tintedWithColor:(UIColor*)color;
+ (UIImage*) roundRectWithCorner:(CGFloat)radius
                 filledWithColor:(UIColor *)fillColor
                strokedWithColor:(UIColor *)strokeColor
                       lineWidth:(CGFloat)lineWidth;
- (UIImage*) imageTintedWithColor:(UIColor *)tintColor;
@end
