//
//  VVTintedImageView.m
//  Pods
//
//  Created by Alexander Darovsky on 05.04.17.
//
//

#import "VVTintedImageView.h"

@implementation VVTintedImageView {
    BOOL __updating;
}

- (void) prepareForInterfaceBuilder
{
    [super prepareForInterfaceBuilder];

    [self __configure];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self __configure];
}

- (void) setImage:(UIImage *)image
{
    [super setImage:image];
    [self __configure];
}

- (void) __configure
{
    if (!__updating) {
        __updating = YES;
        self.image = [self.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        __updating = NO;
    }
}

@end
