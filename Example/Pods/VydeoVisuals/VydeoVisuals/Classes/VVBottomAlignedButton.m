//
//  BottomAlignedButton.m
//  FanDate
//
//  Created by Alexander Darovsky on 07.03.16.
//  Copyright © 2016 Vyu Labs, Inc. All rights reserved.
//

#import "VVBottomAlignedButton.h"

@implementation VVBottomAlignedButton

- (void) awakeFromNib
{
    [super awakeFromNib];

    self.titleLabel.numberOfLines = 0;
    self.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
}

-(void)layoutSubviews {
    [super layoutSubviews];

    CGRect titleLabelFrame = self.titleLabel.frame;
//    CGSize labelSize = [self.titleLabel.text sizeWithFont:self.titleLabel.font constrainedToSize:CGSizeMake(self.frame.size.width, CGFLOAT_MAX) lineBreakMode:NSLineBreakByWordWrapping];
    CGRect labelRect = [self.titleLabel.text boundingRectWithSize:CGSizeMake(self.frame.size.width, CGFLOAT_MAX)
                                                          options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading
                                                       attributes:@{NSFontAttributeName:self.titleLabel.font}
                                                          context:nil];
    CGSize labelSize = labelRect.size;

    CGRect imageFrame = self.imageView.frame;

    CGSize fitBoxSize = (CGSize){.height = labelSize.height + self.topPadding +  imageFrame.size.height, .width = MAX(imageFrame.size.width, labelSize.width)};

    CGRect fitBoxRect = CGRectInset(self.bounds, (self.bounds.size.width - fitBoxSize.width)/2, (self.bounds.size.height - fitBoxSize.height)/2);

    imageFrame.origin.y = fitBoxRect.origin.y;
    imageFrame.origin.x = CGRectGetMidX(fitBoxRect) - (imageFrame.size.width/2);
    self.imageView.frame = imageFrame;

    // Adjust the label size to fit the text, and move it below the image

    titleLabelFrame.size.width = labelSize.width;
    titleLabelFrame.size.height = labelSize.height;
    titleLabelFrame.origin.x = (self.frame.size.width / 2) - (labelSize.width / 2);
    titleLabelFrame.origin.y = fitBoxRect.origin.y + imageFrame.size.height + self.topPadding;
    self.titleLabel.frame = titleLabelFrame;
}

- (CGSize) intrinsicContentSize
{
    CGSize s = [super intrinsicContentSize];

    CGRect labelRect = [self.titleLabel.text boundingRectWithSize:CGSizeMake(s.width, CGFLOAT_MAX)
                                                          options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading
                                                       attributes:@{NSFontAttributeName:self.titleLabel.font}
                                                          context:nil];

    CGSize imageSize = self.imageView.image.size;
    s.height = imageSize.height + self.topPadding + labelRect.size.height;

    return s;
}

- (CGSize) sizeThatFits:(CGSize)targetSize
{
    CGRect labelRect = [self.titleLabel.text boundingRectWithSize:CGSizeMake(targetSize.width, CGFLOAT_MAX)
                                                          options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading
                                                       attributes:@{NSFontAttributeName:self.titleLabel.font}
                                                          context:nil];

    CGSize imageSize = self.imageView.image.size;

    targetSize.width = MAX(self.imageView.frame.size.width, self.titleLabel.frame.size.width);
    targetSize.height = imageSize.height + self.topPadding + labelRect.size.height;
    return targetSize;
}
@end
